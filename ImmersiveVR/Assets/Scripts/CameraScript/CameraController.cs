using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.SpatialTracking;

public class CameraController : MonoBehaviour
{
    private float _counterKinematicPlay = 1.0f;
    private float nextWaitTimeKinematic = 1.0f;
    private float _rotateInput;
    private float rotationSpeed = 40;

    private const float MAXDistance = 8;
    
    private TrackedPoseDriver _trackedPoseDriver;
    public PlayableDirector _playableDirector;
    private EventController _eventController;
    
    private bool _isTrackedPoseDriver = true;
    
    private void Start()
    {
        _trackedPoseDriver = GetComponent<TrackedPoseDriver>();//Obtiene el componente TrackedPoseDriver
    }

    /// <summary>
    /// ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /// </summary>
    void Update()
    {
        //var x = trackedPoseDriver.transform.rotation.y;

        //Genera una cinematica para la rotacion automatica
        _counterKinematicPlay += Time.deltaTime;
        if (Input.GetButtonDown("Fire2"))
        {
            _trackedPoseDriver.enabled = false; //Desactiva el trackedPoseDriver para poder reproducir la cinematica
            _playableDirector.Play(); //Reproduce la cinematica
            _counterKinematicPlay = 0;
        }

        if (_counterKinematicPlay >= nextWaitTimeKinematic) //Verifica que la animacion haya terminado
        {
            if (_isTrackedPoseDriver)
            {
                _trackedPoseDriver.enabled = true; //Activa el trackedPoseDriver
            }
        }

        //Para la rotacion con el mando 
        //if (Input.GetButtonDown("Fire3"))
        if (Input.GetButtonDown("Fire3") || Input.GetKeyDown("q"))
        {
            if (!_isTrackedPoseDriver)
            {
                //var b = trackedPoseDriver.trackingType == TrackedPoseDriver.TrackingType.RotationAndPosition;
                _trackedPoseDriver.enabled = true;
                _isTrackedPoseDriver = true;
            }
            else
            {
                //var b = trackedPoseDriver.trackingType == TrackedPoseDriver.TrackingType.RotationAndPosition;
                _trackedPoseDriver.enabled = false;
                _isTrackedPoseDriver = false;
            }
        }

        if (_isTrackedPoseDriver == false)
        {
            Invoke("RotateHead", 0.1f);
        }
    }

    /// <summary>
    /// ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /// </summary>
    void FixedUpdate()
    {
        RaycastHit hit;
        // Does the ray intersect any objects excluding the player layer
        if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.forward), out hit, MAXDistance))//Si el raycast detecta un objeto
        {
            //Obtiene el objeto el componente (Script) EventController (Se asegura se examine el objeto que posee los evento) a partir del rayo del raycast
            EventController eventCrontroller = hit.transform.GetComponent<EventController>();
            GameObject gameObjectHit = hit.transform.gameObject;
            
            if (eventCrontroller != null && eventCrontroller != this._eventController) { //Determina que ya no son nulos y que el objeto entrante es diferente al anterior entra a ejecutar las funciones de los eventos
                //this.eventCrontroller?.PointerExit(); 
                this._eventController = eventCrontroller;
                eventCrontroller.PointerEnter();
            }
            if (eventCrontroller == null) {//Si no hay objeto eventCrontroller captado con el raycast
                this._eventController?.PointerExit();//Validar que this.eventController esta vacio o no para poder acceder al metodo de la clase PointerExit
                this._eventController = null;
            }
        }
        else //Si no detecta ningun objeto con el raycast
        {
            _eventController?.PointerExit();
            _eventController = null;
        }
    }
    
    private void RotateHead()
    {
        _rotateInput = Input.GetAxis("Horizontal");
        
        this.transform.Rotate(Vector3.up * (rotationSpeed * Time.deltaTime * _rotateInput));//0,Ry,0
    }
}
