﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SpatialTracking;


public class DrawDistance : MonoBehaviour
{
	[Header ("Settings")]
	[Range (0f, 10f)]public float distance;
	public Color color;
	[Space]
	[TextArea] public string note;

	private void OnDrawGizmos()
	{
		Gizmos.color = color;
		Gizmos.DrawWireSphere (transform.position, distance);
	}
}