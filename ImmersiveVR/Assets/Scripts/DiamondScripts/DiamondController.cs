using System;
using System.Collections;
using System.Collections.Generic;
using ItemsScript;
using UnityEngine;

public class DiamondController : MonoBehaviour
{
    private Score _score;

    public void Start()
    {
        _score = GameObject.Find("Player").GetComponent<Score>();
    }

    public void Activate()
    {
        _score.score++;
        Destroy(this.gameObject);
        
    }
}
