using TMPro;
using UnityEngine;

namespace ItemsScript
{
    public class Score : MonoBehaviour
    {
        public int score = 0;
        public TextMeshProUGUI scoreText;
    
        void Update()
        {
            scoreText.text = score.ToString("D");
        }
    }
}
