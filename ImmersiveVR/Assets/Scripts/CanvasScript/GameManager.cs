using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    //Script para la trasicion entre la pantalla de inicio (Made with Unity) y la escena del juego
    public Image ImageFade;
    
    void Start()
    {
       Invoke("Fade",1);
    }

    private void Fade()
    {
        ImageFade.CrossFadeAlpha(0,2,true);
    }
}
