using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class LoadScenesManager : MonoBehaviour
{
    public void Update()
    {
        Debug.Log("Cuantas escenas hay: " + SceneManager.sceneCountInBuildSettings );
    }

    public void HomeSceneLoad()
    {
        SceneManager.LoadSceneAsync("CabinScene");
    }

    public void MainSceneLoad()
    {
        SceneManager.LoadSceneAsync("ForestScene");
    }
}
