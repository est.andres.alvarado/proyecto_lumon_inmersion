using System;
using System.Collections;
using System.Collections.Generic;
using System.Net.Mime;
using TMPro;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.SpatialTracking;
using UnityEngine.UI;


public class PlayerController : MonoBehaviour
{
    //Propiedades
    //[HideInInspector]
    [Range(-20, 20),
     SerializeField,
     Tooltip("Velocidad actual del player")]
    //Modificar de Rango de la variable speed (visible en Unity), Ser visible en Unity
    private float playerSpeed = 10.0f;

    [Range(-360, 360),
     SerializeField,
     Tooltip("Angulo de rotacion")]
    //Modificar de Rango de la variable speed (visible en Unity), Ser visible en Unity
    private float rotateAngle;

    private float _horizontalInput;
    private float _verticalInput;

    private float _countOutOfBound = 0;
    private const float TimeOutToResetPosition = 5.0f;

    private readonly float jumpForce = 10;
    private readonly float gravityMultiplier = 2;


    private bool _isOnGround = true;
    private bool _isOnBound = true;
    private bool _isTurn = true;
    private bool _rotateWithTheHead = true; //Para hacer la rotacion desde los controles y no desde la cabeza

    private float _timer = 5.0f;
    public TextMeshProUGUI _timerText;
    public GameObject _uiWarning;

    private Vector3 _initialPlayerPosition;

    //Componentes
    private Rigidbody _playerRb;

    void Start()
    {
        _playerRb = GetComponent<Rigidbody>(); //Obtiene el rigidbody de player controller
        Physics.gravity *= gravityMultiplier; //Modifica la aceleracion de la gravedad para hacer el salto mas realista
        _initialPlayerPosition = this.transform.position;
    }

    void Update()
    {
        Invoke("CheckBound", 0.1f); //Chequea si el jugador sale de los borde del mapa

        Invoke("Move", 0.1f);

        //if (Input.GetButtonDown("Fire3"))
        if (Input.GetButtonDown("Fire3") || Input.GetKeyDown("q"))
        {
            if (!_rotateWithTheHead)
            {
                _rotateWithTheHead = true;
            }
            else
            {
                _rotateWithTheHead = false;
            }
        }
        

        //Controles de salto y rotacion automatica
        if (Input.GetButtonDown("Fire1")) //Presiona un gatillo del control para saltar
        {
            Invoke("Jump", 0.1f);
        }

        if (Input.GetButtonDown("Fire2")) //Preciona la letra "c" del control para hacer una rotacion automatica
        {
            Invoke("RotationHead", 0.1f);
        }
    }

    private void Move()
    {
        _verticalInput = Input.GetAxis("Vertical");
        _horizontalInput = Input.GetAxis("Horizontal");

        if (_isTurn)
        {
            this.transform.Translate(Camera.main.transform.forward *
                                     (playerSpeed * Time.deltaTime * _verticalInput)); //0,0,1
        }
        else
        {
            this.transform.Translate(
                Camera.main.transform.forward * (-1 * playerSpeed * Time.deltaTime * _verticalInput)); //0,0,1
        }

        if (_rotateWithTheHead)
        {
            if (_isTurn)
            {
                this.transform.Translate(Camera.main.transform.right *
                                         (playerSpeed * Time.deltaTime * _horizontalInput)); //1,0,0
            }
            else
            {
                this.transform.Translate(Camera.main.transform.right *
                                         (-1 * playerSpeed * Time.deltaTime * _horizontalInput)); //1,0,0
            }
        }
    }

    private void Jump()
    {
        if (_isOnGround)
        {
            _playerRb.AddForce(Vector3.up * jumpForce, ForceMode.Impulse); //F = m*a 0,1,0
            _isOnGround = false;
        }
    }

    private void RotationHead()
    {
        //Rotacion automatica 180°
        if (_isTurn)
        {
            this.transform.rotation *= Quaternion.AngleAxis(rotateAngle, Vector3.up);
            _isTurn = false;
        }
        else if (!_isTurn)
        {
            this.transform.rotation *= Quaternion.AngleAxis(rotateAngle, Vector3.up);
            _isTurn = true;
        }
    }

    private void CheckBound() //Mensaje cuando el player sale de las fronteras
    {
        if (!_isOnBound)
        {
            _uiWarning.SetActive(true);
            _timer -= Time.deltaTime;
            _countOutOfBound += Time.deltaTime;
            _timerText.text = _timer.ToString("F1");
        }
        else
        {
            _uiWarning.SetActive(false);
            _countOutOfBound = 0;
            _timer = 5.0f;
        }

        if (_countOutOfBound >= TimeOutToResetPosition) //Tiempo para resetear la posicion del jugador
        {
            _uiWarning.SetActive(false);
            this.transform.position = _initialPlayerPosition;
            _isOnBound = true;
            _countOutOfBound = 0;
            _timer = 5.0f;
        }
    }

    /// <summary>
    /// Comparacion con colisiones
    /// </summary>

    private void OnCollisionEnter(Collision other) //Ver si entra en contacto el player con otro (el suelo)
    {
        if (other.gameObject.CompareTag("Ground"))
        {
            _isOnGround = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Bound"))
        {
            _isOnBound = false;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Bound"))
        {
            _isOnBound = true;
        }
    }
}