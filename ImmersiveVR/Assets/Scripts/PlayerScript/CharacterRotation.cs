using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterRotation : MonoBehaviour
{
    public Transform _target;
    void Update()
    {
        var euler = _target.rotation.eulerAngles; 
        var rot = Quaternion.Euler(0, euler.y, 0); 
        this.transform.rotation = rot;
    }
}
