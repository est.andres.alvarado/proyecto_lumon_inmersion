using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

public class TimelineController : MonoBehaviour
{
    public PlayableDirector playableDirector;
    public bool activateKinematics;
    
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player") || collision.CompareTag("Bus"))
        {
            if (activateKinematics == false)
            {
                playableDirector.Play();
                activateKinematics = true;
            }
        }
    }
}
