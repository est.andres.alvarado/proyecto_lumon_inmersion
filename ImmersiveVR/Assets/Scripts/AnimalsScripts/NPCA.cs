using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPCA : MonoBehaviour
{
     public AudioSource[] voiceLine;
     public int lineNumber;

    void OnTriggerEnter(Collider other)
    {
        if(other.tag == "NPC")
        {
            StartCoroutine(NPCVoiceover(0));
        }
        if (other.tag == "Ave1")
        {
            StartCoroutine(NPCVoiceover(1));
        }
        if (other.tag == "Mar")
        {
            StartCoroutine(NPCVoiceover(2));
        }
        if (other.tag == "ard")
        {
            StartCoroutine(NPCVoiceover(3));
        }
    }
    IEnumerator NPCVoiceover(int entrada)
    {
        this.gameObject.GetComponent<BoxCollider>().enabled = false;
        voiceLine[entrada].Play();
        yield return new WaitForSeconds(2);
        this.gameObject.GetComponent<BoxCollider>().enabled = true;
    }
}
