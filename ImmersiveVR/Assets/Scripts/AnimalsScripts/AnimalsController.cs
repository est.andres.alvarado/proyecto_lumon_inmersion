using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
public class AnimalsController : MonoBehaviour
{
    //Objetos para cumplir objetivos
    public Transform Objetivo;
    public NavMeshAgent agente;
    


    // Start is called before the first frame update
    void Start()
    {
        agente = GetComponent<NavMeshAgent>();

    }

    // Update is called once per frame
   void Update()
    {
        agente.destination = Objetivo.position;

    }
    
}
