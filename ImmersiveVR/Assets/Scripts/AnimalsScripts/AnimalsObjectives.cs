using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimalsObjectives : MonoBehaviour
{
    public int trigNu;

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Dino")
        {
            if (trigNu == 4)
            {

                trigNu = 0;

            }
            if (trigNu == 3)
            {

                this.gameObject.transform.position = new Vector3(-39, 1, -3);
                trigNu = 4;

            }


            if (trigNu == 2)
            {

                this.gameObject.transform.position = new Vector3(-22, 1, -4);
                trigNu = 3;

            }
            if (trigNu == 1)
            {

                this.gameObject.transform.position = new Vector3(-28, 1, 54);
                trigNu = 2;

            }

            if (trigNu == 0)
            {

                this.gameObject.transform.position = new Vector3(-52, 1, 54);
                trigNu = 1;

            }

        }
        if (other.tag == "Ave1")
        {
            if (trigNu == 4)
            {

                trigNu = 0;

            }
            if (trigNu == 3)
            {

                this.gameObject.transform.position = new Vector3(-12, 2, 2);
                trigNu = 4;

            }


            if (trigNu == 2)
            {

                this.gameObject.transform.position = new Vector3(-21, 2, -46);
                trigNu = 3;

            }
            if (trigNu == 1)
            {

                this.gameObject.transform.position = new Vector3(42, 2, -46);
                trigNu = 2;

            }

            if (trigNu == 0)
            {

                this.gameObject.transform.position = new Vector3(42, 2, 2);
                trigNu = 1;

            }
        }
        if (other.tag == "Tor")
        {
            if (trigNu == 4)
            {

                trigNu = 0;

            }
            if (trigNu == 3)
            {

                this.gameObject.transform.position = new Vector3(-113, 0, -68);
                trigNu = 4;

            }


            if (trigNu == 2)
            {

                this.gameObject.transform.position = new Vector3(94, 0, -5);
                trigNu = 3;

            }
            if (trigNu == 1)
            {

                this.gameObject.transform.position = new Vector3(22, 0, 89);
                trigNu = 2;

            }

            if (trigNu == 0)
            {

                this.gameObject.transform.position = new Vector3(-147, 0, 89);
                trigNu = 1;

            }
        }
        if (other.tag == "Mar")
        {
            if (trigNu == 4)
            {

                trigNu = 0;

            }
            if (trigNu == 3)
            {

                this.gameObject.transform.position = new Vector3(36, 2, 63);
                trigNu = 4;

            }


            if (trigNu == 2)
            {

                this.gameObject.transform.position = new Vector3(55, 2, 52);
                trigNu = 3;

            }
            if (trigNu == 1)
            {

                this.gameObject.transform.position = new Vector3(52, 2, 39);
                trigNu = 2;

            }

            if (trigNu == 0)
            {

                this.gameObject.transform.position = new Vector3(36, 2, 49);
                trigNu = 1;

            }
        }
        if (other.tag == "ard")
        {
            if (trigNu == 4)
            {

                trigNu = 0;

            }
            if (trigNu == 3)
            {

                this.gameObject.transform.position = new Vector3(-24, 2, 46);
                trigNu = 4;

            }


            if (trigNu == 2)
            {

                this.gameObject.transform.position = new Vector3(-11, 2, 6);
                trigNu = 3;

            }
            if (trigNu == 1)
            {

                this.gameObject.transform.position = new Vector3(20, 2, 20);
                trigNu = 2;

            }

            if (trigNu == 0)
            {

                this.gameObject.transform.position = new Vector3(16 , 2, 61);
                trigNu = 1;

            }
        }
     }
}
