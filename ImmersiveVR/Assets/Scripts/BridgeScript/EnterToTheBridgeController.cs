using ItemsScript;
using UnityEngine;
using UnityEngine.Serialization;

namespace BridgeScript
{
    public class EnterToTheBridgeController : MonoBehaviour
    {
        private Score _score;
        private int diamondCount = 10;
        void Start()
        {
            _score = GameObject.Find("Player").GetComponent<Score>();
        }

        void Update()
        {
            if (_score.score == diamondCount)
            {
                Destroy(this.gameObject);
                Debug.Log("Se abre la entrada del puente");
            }
        }
    }
}
